//GET ALL TASKS
document.getElementById('all').addEventListener('change', getTasksEventListener);
document.getElementById('ongoing').addEventListener('change', getTasksEventListener);
document.getElementById('completed').addEventListener('change', getTasksEventListener);

let activeMode;

function getTasksEventListener() {
    const parent = document.getElementById('allTasks');
    if (parent.hasChildNodes()) updateAllTasks();
    let mode = this.id;
    activeMode = this.id;
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'user_id': userData._id,
            'status': mode,
            'Authorization': 'Bearer ' + userData.token
        }
    };
    fetch('tasks/', options)
        .then(res => res.json())
        .then(res => {
            console.log(res);
            listAllTasks(res);
        }).catch(err => {
            console.log(err);
        });
}

//GET ONE TASK
function taskDetailsEventListener() {
    let parent = document.getElementById('getTask');
    if (parent.style.display === 'block') {
        parent.style.display = 'none';
        return;
    }
    parent.style.display = 'block';
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'user_id': userData._id,
            'Authorization': 'Bearer ' + userData.token
        }
    };
    fetch('tasks/' + this.id, options)
        .then(res => res.json())
        .then(res => {
            taskWindow(res);
        }).catch(err => {
            console.log(err);
        });
}
//PATCH TASK
function patchThisTask() {
    document.getElementById('cross').onclick();
    const options = {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            'user_id': userData._id,
            'Authorization': 'Bearer ' + userData.token
        },
        body: JSON.stringify({
            name: document.getElementById('name').innerHTML,
            status: document.getElementById('status').checked,
            details: document.getElementById('details').innerHTML
        })
    };
    fetch('tasks/' + this.id, options)
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (activeMode === 'completed' && document.getElementById('status').checked === true) {
                document.getElementById(activeMode).dispatchEvent(new Event("change"));
                updateAllTasks();
            } else if (activeMode === 'ongoing' && document.getElementById('status').checked === false) {
                document.getElementById(activeMode).dispatchEvent(new Event("change"));
                updateAllTasks();
            } else if (activeMode === 'all') {
                document.getElementById(activeMode).dispatchEvent(new Event("change"));
                updateAllTasks();
            } else {
                removeTask(this.id);
            }
        }).catch(err => {
            console.log(err);
        });
}
//DELETE TASK
function deleteThisTask() {
    if (!confirm('Opravdu chcete smzat tento ukol?')) return;
    const options = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'user_id': userData._id,
            'Authorization': 'Bearer ' + userData.token
        }
    };
    fetch('tasks/' + this.id, options)
        .then(res => res.json())
        .then(res => {
            alert('Ukol úspěšně odstraněn!');
            console.log(res);
            removeTask(this.id);
        }).catch(err => {
            console.log(err);
        });
}

function deleteAllTasks() {
    const options = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'user_id': userData._id,
            'Authorization': 'Bearer ' + userData.token
        }
    };
    fetch('tasks/', options)
        .then(res => res.json())
        .then(res => {
            console.log(res);
        }).catch(err => {
            console.log(err);
        });
}

//CREATE NEW TASK
document.getElementById('createTask').onclick = () => {
    const name = document.getElementById('newName').value;
    if (!name) return;
    const details = document.getElementById('newDetails').value;
    document.getElementById('newDetails').value = '';
    document.getElementById('newName').value = '';
    document.getElementById('addNewTask').onclick();
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'user_id': userData._id,
            'Authorization': 'Bearer ' + userData.token
        },
        body: JSON.stringify({
            name: name,
            details: details
        })
    };
    fetch('tasks/', options)
        .then(res => res.json())
        .then(res => {
            console.log(res);
            if (activeMode !== 'completed') {
                isdbEmpty(false);
                appendTask(res);
            }
        }).catch(err => {
            console.log(err);
        });
}

function removeTask(id) {
    overlay(false);
    document.getElementById('getTask').style.display = 'none';
    document.getElementById(id).parentElement.remove();
    if (!document.getElementById('allTasks').hasChildNodes()) isdbEmpty(true);
}

function updateAllTasks() {
    let parent = document.getElementById('allTasks');
    while (parent.lastChild) {
        parent.removeChild(parent.lastChild);
    }
}

function listAllTasks(res) {
    if (res.tasks) {
        isdbEmpty(false);
        res.tasks.forEach(e => {
            appendTask(e);
        });
    } else isdbEmpty(true);
}

function isdbEmpty(res) {
    let m = document.getElementById("messageBox");
    if (res) {
        m.style.display = 'block';
        m.innerHTML = 'Žádný nový úkol';
    } else {
        m.innerHTML = '';
        m.style.display = 'none';
    }
}