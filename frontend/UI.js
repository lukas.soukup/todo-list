//zavira okno jednoho ukolu pri kliknuti mimo div
document.addEventListener('click', function(e) {
    var getTask = document.querySelector("#getTask");
    var cross = document.getElementById('cross');
    if (e.target.closest("#getTask")) return;
    if (getTask.style.display !== '' && getTask.style.display !== 'none') {
        cross.onclick();
        getTask.style.display = 'none';
        if (e.target !== document.getElementById('addNewTask')) overlay(false);
    }
});


//pridava ukol do seznamu vsech ukolu
function appendTask(res) {
    let parent = document.getElementById('allTasks');
    let child = document.createElement('div');
    child.id = 'singleTask'
    let name = document.createElement('h1');
    name.innerHTML = res.name;
    let status = document.createElement('h3');
    if (res.status) {
        status.innerHTML = 'HOTOVÝ';
        status.style.backgroundColor = 'green';
    } else {
        status.innerHTML = 'Pracuji na tom';
        status.style.backgroundColor = 'orange';
    }
    let details = document.createElement('button');
    details.innerHTML = 'details';
    details.id = res._id;
    details.addEventListener('click', taskDetailsEventListener);
    child.appendChild(name);
    child.appendChild(status);
    child.appendChild(details);
    parent.appendChild(child);
}

//vytvori div pro zobrazeni jedno vybraneho ukolu
function taskWindow(res) {
    let parent = document.getElementById('getTask');
    let settings = document.getElementById('settings');
    let check = document.getElementsByName('check')[0];
    let cross = document.getElementById('cross');
    let deleteTask = document.getElementsByName('del')[0];
    let name = document.getElementById('name');
    let checkBox = document.getElementById('status');
    let status = document.getElementById('statusText');
    let details = document.getElementById('details');
    let id = document.getElementById('id');
    check.id = res._id;
    deleteTask.id = res._id;
    name.innerHTML = res.name;
    id.innerHTML = 'task id: ' + res._id;
    parent.style.display = 'block';
    check.style.display = 'none';
    cross.style.display = 'none';
    checkBox.style.display = 'none';
    checkBox.checked = res.status;
    if (res.details) {
        details.innerHTML = res.details;
    } else {
        details.innerHTML = "no entry";
    }

    checkBox.onchange = () => {
        if (checkBox.checked) {
            status.innerHTML = 'HOTOVÝ';
            status.style.backgroundColor = 'green';
        } else {
            status.innerHTML = 'Pracuji na tom';
            status.style.backgroundColor = 'orange';
        }
    }
    checkBox.onchange();

    settings.onclick = function() {
        this.style.display = 'none'
        check.style.display = 'block';
        cross.style.display = 'block';
        name.style.outline = 'blue solid 1px';
        details.style.outline = 'blue solid 1px';
        status.style.outline = 'blue solid 1px';
        status.htmlFor = 'status';
        name.contentEditable = true;
        details.contentEditable = true;
    }
    cross.onclick = function(e) {
        settings.style.display = 'block'
        check.style.display = 'none';
        this.style.display = 'none';
        status.htmlFor = '';
        name.contentEditable = false;
        details.contentEditable = false;
        name.style.outline = 'none';
        details.style.outline = 'none';
        status.style.outline = 'none';
        if (!e) return;
        checkBox.checked = res.status;
        checkBox.onchange();
        name.innerHTML = res.name;
        if (res.details) {
            details.innerHTML = res.details;
        } else {
            details.innerHTML = "no entry";
        }
    }
    overlay(true);
    check.addEventListener('click', patchThisTask);
    deleteTask.addEventListener('click', deleteThisTask);
}

document.getElementById('addNewTask').onclick = function() {
    let post = document.getElementById('postTask');
    let task = document.getElementById('getTask');
    if (post.style.display === 'none' || post.style.display === '') {
        post.style.display = 'block';
        this.innerHTML = 'Zavřít';
        overlay(true);
    } else {
        post.style.display = 'none';
        this.innerHTML = 'Přidat';
        overlay(false);
    }
}

function overlay(power) {
    let cover = document.getElementById('cover');
    cover.style.backgroundColor = 'black';
    if (power) {
        cover.style.opacity = '0.8';
        cover.style.zIndex = '5';
    } else {
        cover.style.opacity = '0';
        cover.style.zIndex = '-1';
    }
}