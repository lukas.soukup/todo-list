//how to upload api with mongodb to heroku
//https://jasonwatmore.com/post/2018/12/06/deploy-to-heroku-node-mongo-api-for-authentication-registration-and-user-management
const loginBtn = document.getElementById('submitLogin');
const signupBtn = document.getElementById('submitSignup');
let userData = {};

loginBtn.onclick = () => {
    userOps('login');
}
signupBtn.onclick = () => {
    userOps('signup');
}

function userOps(op) {
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    let info = document.getElementById('infoBox');
    if (!email || !password) {
        info.innerHTML = 'Vyplň email i heslo!';
        return;
    }
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    };
    fetch('user/' + op, options)
        .then(res => res.json())
        .then(res => {
            console.log(res.message);
            if (!res.message) info.innerHTML = 'Neplatný email';
            else info.innerHTML = res.message;
            if (op === 'login') {
                userData = {
                    _id: res.user_id,
                    token: res.token
                };
                if (res.message === 'Login successful') {
                    document.getElementById('user').innerHTML = email;
                    document.getElementById('all').dispatchEvent(new Event("change"));
                    overlay(false);
                    document.getElementById('signupLoginBox').style.display = 'none';
                }
            }
        }).catch(err => {
            console.log(err);
        });
}

document.getElementById('deleteUser').onclick = () => {
    if (!confirm('Opravdu chcete smazat Váš účet?')) return;
    const options = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + userData.token
        }
    }
    fetch('user/' + userData._id, options)
        .then(res => res.json())
        .then(res => {
            userData = {};
            document.getElementById('email').value = '';
            document.getElementById('password').value = '';
            console.log(res.message);
            if (res.message === 'User deleted') {
                updateAllTasks();
                alert('Účet smazán');
            } else alert('Přihlaš se!');
        }).catch(err => {
            console.log(err);
        });
    deleteAllTasks();
    document.getElementById('logout').onclick();
}
document.getElementById('logout').onclick = () => {
    let cover = document.getElementById('cover');
    cover.style.opacity = '1';
    cover.style.zIndex = '8';
    cover.style.backgroundColor = 'rgb(78, 71, 71)';
    userData = {};
    document.getElementById('user').innerHTML = '';
    document.getElementById('infoBox').innerHTML = 'Jsi odhlášen';
    document.getElementById('email').value = '';
    document.getElementById('password').value = '';
    document.getElementById('signupLoginBox').style.display = 'block';
    updateAllTasks();
}