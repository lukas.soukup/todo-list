const mongoose = require('mongoose');
//toto je pouze schema jak by mel vypadat a ukladat se do DB objekt task
const taskSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    user_id: String,
    name: { type: String, required: true },
    status: { type: Boolean, default: false },
    details: String
});

module.exports = mongoose.model('Task', taskSchema);