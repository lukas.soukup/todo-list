const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const TasksController = require('../controllers/tasksController');

//vraci list vsech ukolu
router.get('/', checkAuth, TasksController.tasks_get_all);

//vklada novy ukol
router.post('/', checkAuth, TasksController.tasks_create_new);

//vraci jeden specificky ukol
router.get('/:taskId', checkAuth, TasksController.tasks_get_one);

//upravuje jeden specificky ukol
router.patch('/:taskId', checkAuth, TasksController.tasks_patch_task);

//maze jeden specificky ukol
router.delete('/:taskId', checkAuth, TasksController.tasks_delete_task);

//maze vsechny zaznamy ucivatele
router.delete('/', checkAuth, TasksController.tasks_delete_all);

module.exports = router;