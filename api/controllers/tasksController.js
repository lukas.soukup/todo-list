const Task = require('../models/taskModel');
const User = require('../models/userModel');
const mongoose = require('mongoose');
const port = process.env.PORT || 3000;

exports.tasks_get_all = (req, res, next) => {
    const user_id = req.headers.user_id;
    const status = req.headers.status;
    if (!user_id) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }
    let what;
    if (status == 'completed') what = { user_id: user_id, status: true };
    else if (status == 'ongoing') what = { user_id: user_id, status: false };
    else what = { user_id: user_id };
    Task.find(what)
        .select('_id name status')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                tasks: docs.map(doc => {
                    return {
                        name: doc.name,
                        status: doc.status,
                        _id: doc._id,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:' + port + '/tasks/' + doc._id
                        }
                    }
                })
            };
            if (docs.length == 0) {
                res.status(200).json({ message: "Database is empty!" });
            } else {
                res.status(200).json(response);
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ Error: err });
        })
};

exports.tasks_create_new = (req, res, next) => {
    const user_id = req.headers.user_id;
    if (!user_id) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }
    const task = new Task({
        _id: new mongoose.Types.ObjectId(), //auto generuje id, aby vzdy bylo rozdilne
        user_id: user_id,
        name: req.body.name,
        //status: req.body.status,
        details: req.body.details
    });
    //uklada vytvoreny ukol/task do MongoDB
    task.save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Task created!",
                name: result.name,
                status: result.status,
                details: result.details,
                user_id: result.user_id,
                _id: result._id,
                request: {
                    type: 'GET',
                    url: 'http://localhost:' + port + '/tasks/' + result._id
                }
            })
        }).catch(err => {
            console.log(err);
            res.status(500).json({
                eror: err
            });
        });
};

exports.tasks_get_one = (req, res, next) => {
    const id = req.params.taskId;
    const user_id = req.headers.user_id;
    if (!user_id) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }
    Task.find({ user_id: user_id, _id: id })
        .exec()
        .then(doc => {
            if (doc != null) {
                res.status(200).json({
                    name: doc[0].name,
                    status: doc[0].status,
                    details: doc[0].details,
                    _id: doc[0]._id,
                    request: {
                        type: 'GET',
                        descripcion: 'Get list of all Tasks',
                        url: 'http://localhost:' + port + '/tasks'
                    }
                });
            } else {
                res.status(404).json({
                    message: "Task not found"
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ eror: err });
        });
};

exports.tasks_patch_task = (req, res, next) => {
    const user_id = req.headers.user_id;
    if (!user_id) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }
    const id = req.params.taskId;
    const updateOps = {};
    var keys = [];
    //vstup: { "key": "newValue", "key": "newValue", ...}
    for (var x in req.body) {
        keys.push(x); // obsahuje klice vech pozadovanych zmen v elementů
    }
    for (let i = 0; i < Object.keys(req.body).length; i++) {
        updateOps[keys[i]] = req.body[keys[i]]; // vystup je { name: 'Migrating to Japan', status: false }
    }
    Task.update({ _id: id, user_id: user_id }, { $set: updateOps })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Task updated!',
                request: {
                    type: 'GET',
                    url: 'http://localhost:' + port + '/tasks/' + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ Error: err });
        })
};

exports.tasks_delete_task = (req, res, next) => {
    const user_id = req.headers.user_id;
    if (!user_id) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }
    const id = req.params.taskId;
    Task.remove({ _id: id, user_id: user_id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Task deleted!',
                request: {
                    type: 'GET',
                    descripcion: 'Get list of all Tasks',
                    url: 'http://localhost:' + port + '/tasks'
                }
            });
        })
        .catch(err => {
            res.status(500).json({ Error: err });
        });
};

exports.tasks_delete_all = (req, res, next) => {
    const user_id = req.headers.user_id;
    if (!user_id) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }
    Task.remove({ user_id: user_id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'All user\'s data deleted',
                request: {
                    type: 'GET',
                    descripcion: 'Get list of all Tasks',
                    url: 'http://localhost:' + port + '/tasks'
                }
            });
        })
        .catch(err => {
            res.status(500).json({ Error: err });
        });
};