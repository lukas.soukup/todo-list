const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const taskRoutes = require('./api/routes/tasksRoute');
const userRoutes = require('./api/routes/userRounte');
const mongoose = require('mongoose');
const path = require('path');
const envVar = require('dotenv').config({ path: __dirname + '/.env' });

mongoose.connect("mongodb+srv://lukas_soukup:" + process.env['MONGO_ATLAS_PW'] + "@weap-projekt-r2w2c.mongodb.net/test?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.use(express.static('frontend'));
app.get('/', function(req, res) {
    res.sendFile('index.html', { root: path.join(__dirname, '/frontend') });
});
app.get('/style.css', function(req, res) {
    res.sendFile('style.css', { root: path.join(__dirname, '/frontend') });
});
app.get('/tasks_manager.js', function(req, res) {
    res.sendFile('tasks_manager.js', { root: path.join(__dirname, '/frontend') });
});
app.get('/user_manager.js', function(req, res) {
    res.sendFile('user_manager.js', { root: path.join(__dirname, '/frontend') });
});
app.get('/UI.js', function(req, res) {
    res.sendFile('UI.js', { root: path.join(__dirname, '/frontend') });
});
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//course error handeling 
//umoznuje odesilat req z SPA, atd...
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*"); //umoznuje pristup pro vsechny clienty
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-type, Accept, Authorization"); // umozni pristup pouze uvedenym headerum

    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, PATCH, DELETE");
        return res.status(200).json({});
    }
    next(); //pro preposlani do dalsiho routeru
});
app.use('/tasks', taskRoutes); //tohle preposle vsechny requesty mireny na taks/ukoly do souboru tasks.js
app.use('/user', userRoutes);
//middleware ktery odchytava requesty mirene na neexistujci odkaz
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

//middleware ktery odchytava vsechny ostatni errory
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});
module.exports = app;